﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Private Variables
    private float speed = 20.0f;
    private float turnSpeed = 60.0f;
    private float horizontalInput;
    private float forwardInput;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()

    {
        //PlayerInput

        horizontalInput = Input.GetAxis("Horizontal");
        forwardInput = Input.GetAxis("Vertical");


        
        // Moves the car forward when vertical keys are pressed
        transform.Translate(Vector3.forward * Time.deltaTime * speed * forwardInput);

        //Rotates the car when horizontal keys are pressed
        transform.Rotate(Vector3.up, turnSpeed * horizontalInput  * Time.deltaTime);

    }
}
